<?php include 'templates/header.tpl.php' ?>

    <div class="submit-form-holder">
        <?php include 'templates/form.tpl.php' ?>
    </div>

    <form class="search-form" action="">
        <fieldset>
            <div class="row">
                <div class="col-lg-5">
                    <div class="input-group bar">
                      <input class="search form-control" type="search" placeholder="Search">
                      <span class="input-group-btn">
                        <button class="btn submit" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                      </span>
                    </div>
                </div>
            </div>


            <div class="results-table-holder">
                <?php include 'templates/table.tpl.php' ?>
            </div>
        </fieldset>
    </form>

<?php include 'templates/footer.tpl.php' ?>