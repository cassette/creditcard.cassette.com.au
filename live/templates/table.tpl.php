<?php if (is_array($table)) : ?>
<div class="table-responsive">
    <table class="table table-striped" id="table">
       <thead>
       <tr class="head">
            <th rel="date">Date <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
            <th rel="name">Full Name <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
            <th rel="card">Card Type <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
            <th rel="amount">Amount <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
            <th rel="number">Job Number <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
            <th class="disabled" rel="details">Details <a href="#"><span class="glyphicon glyphicon-chevron-down"></span><span class="glyphicon glyphicon-chevron-up"></span></a></th>
        </tr>
        </thead>
        <tbody> 
        <?php foreach ($table as $row) : ?>
        <tr>
            <td><?php echo $row['date']?></td>
            <td><?php echo $row['name']?></td>
            <td><?php echo $row['type']?></td>
            <td class="search"><?php echo $row['amount']?></td>
            <td><?php echo $row['job_number']?></td>
            <td class="search"><?php echo $row['details']?></td>
        </tr>
        <?php endforeach; ?>
        </tbody> 
    </table>
</div>
<?php endif; ?>