<form class="card-form" action="" method="post">
    <fieldset>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <img src="img/cassette.jpg" class="cassette">
                <h1>Credit Card Log</h1>
                <h2>New Transaction</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <input id="datepicker" type="text" class="form-control" name="<?php echo field_name('date') ?>" value="<?php echo field_value('date'); ?>" placeholder="Date">
            </div>
            <div class="col-md-4">
                <p>Optional - if date note entered, it will submit the current date and time</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <input type="text" class="form-control" name="<?php echo field_name('name') ?>" value="<?php echo field_value('name'); ?>" placeholder="Full Name*">
            </div>
            <div class="col-md-4 error-holder">
                <?php if (!field_valid('name')) : ?>
                <span class="label label-danger">This field is required</span>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <!--<label for="type">Card Type</label>-->
                <select class="form-control" id="type" name="<?php echo field_name('type') ?>">
                    <option value="mastercard" <?php echo field_selected('type', 'mastercard'); ?>>Mastercard</option>
                    <option value="visa" <?php echo field_selected('type', 'visa'); ?>>Visa</option>
                    <option value="amex" <?php echo field_selected('type', 'amex'); ?>>Amex</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="<?php echo field_name('amount') ?>" value="<?php echo field_value('amount'); ?>" placeholder="Amount*">
                    </div>
            </div>
            <div class="col-md-4 error-holder">
                <?php if (!field_valid('amount')) : ?>
                <span class="label label-danger">This field is required and should be numeric</span>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <input type="text" class="form-control" name="<?php echo field_name('job_number') ?>" value="<?php echo field_value('job_number'); ?>" placeholder="Job Number">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <textarea cols="30" class="form-control" rows="10" name="<?php echo field_name('details') ?>" placeholder="Details*"><?php echo field_value('details'); ?></textarea>
            </div>
            <div class="col-md-4 error-holder">
                <?php if (!field_valid('details')) : ?>
                <span class="label label-danger">This field is required</span>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <input class="btn submit form-control" type="submit" value="Submit">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="submit-form-results"></div>
            </div>
        </div>
    </fieldset>
</form>