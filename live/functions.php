<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');

function field_name($name) {
    return "form_data[{$name}]";
}


function field_value($name, $default = '') {
    return isset($_POST['form_data'][$name]) ? $_POST['form_data'][$name] : $default;
}

function field_selected($name, $value) {
    $current = isset($_POST['form_data'][$name]) ? $_POST['form_data'][$name] : '';

    if ($current == $value) {
        return ' selected="selected"';
    }

    return '';
}

function field_valid($name) {
    if (!isset($_POST['form_data'])) return true;

    switch ($name) {
        case 'name':
        case 'details':
            return validation_notempty(field_value($name));

        case 'amount':
            return validation_number(field_value($name));
    }

    return true;
}

function validation_notempty($value) {
    if ($value == 'Full Name*'){
        return false;
    }
    if ($value == 'Details*'){
        return false;
    }
    return !empty($value);
}

function validation_number($value) {
    return is_numeric($value);
}