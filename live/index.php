<?php

define('PROTECT_INCLUDES', true);

require 'config.php';
require 'functions.php';
require 'validation.php';
require 'get_table_data.php';

if (isset($_POST['ajax']) && $_POST['ajax'] == 'from') {
    include 'templates/form.tpl.php';
    die();
}

if (isset($_POST['ajax']) && $_POST['ajax'] == 'table') {
    include 'templates/table.tpl.php';
    die();
}

include 'templates/index.tpl.php';