<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');

$GLOBALS['config']['db']['host'] = 'localhost';
$GLOBALS['config']['db']['name'] = 'creditcardlog';
$GLOBALS['config']['db']['user'] = 'root';
$GLOBALS['config']['db']['pass'] = '';
$GLOBALS['config']['db']['table'] = 'credit_card_log';

$GLOBALS['config']['mailer']['from']['email'] = 'admin@example.com';
$GLOBALS['config']['mailer']['from']['name'] = 'admin';
$GLOBALS['config']['mailer']['to']['email'] = 'admin@example.com';
$GLOBALS['config']['mailer']['to']['name'] = 'admin';
$GLOBALS['config']['mailer']['subject'] = 'Credit Card Log - New Entry';

