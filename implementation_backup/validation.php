<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');

if (isset($_POST['form_data'])) {
    $fields = $_POST['form_data'];

    if (!isset($fields['date']) || empty($fields['date'])) {
        $fields['date'] = date('d.m.Y');
    }

    foreach ($fields as $field => $value) {
        if (!field_valid($field)) {
            return;
        }
    }

    include 'send.php';

    include 'save_in_db.php';

    die();
}