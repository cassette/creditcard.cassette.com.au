jQuery(function(){
    jQuery('.submit-form-holder form').submit(submit_ajax);
});

function submit_ajax() {
        var form = jQuery(this);
        var form_holder = jQuery(this).closest('.submit-form-holder');
        var results_holder = form_holder.find('.submit-form-results');
        var form_data = form.serialize();
        var table_holder = jQuery('.results-table-holder');


        form_holder.find("form :input").prop("disabled", true);
        form_holder.find("form").css({opacity: 0.4});

        form.find('.error-holder').empty();

        results_holder.empty().hide();

        jQuery.ajax({
            url : form.attr('action'),
            type : form.attr('method'),
            data : 'ajax=from&' + form_data,

            success : function(response) {
                if (jQuery('<div></div>').html(response).find('form').length) {
                    form_holder.html(response);

                    form_holder.find('form').submit(submit_ajax);
                } else {
                    form[0].reset();
                    results_holder.html(response);
                    results_holder.slideDown();

                    jQuery.ajax({
                        url : form.attr('action'),
                        type : form.attr('method'),
                        data : 'ajax=table',

                        success: function(response){
                            table_holder.html(response);
                        }
                    });
                }
            },

            complete: function() {
                form_holder.find("form :input").prop("disabled", false);
                form_holder.find("form").css({opacity: 1});
            }
        });

        return false;
}