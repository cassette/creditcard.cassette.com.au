<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');



require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer();
$mail->setFrom($GLOBALS['config']['mailer']['from']['email'], $GLOBALS['config']['mailer']['from']['name']);
$mail->addReplyTo($GLOBALS['config']['mailer']['from']['email'], $GLOBALS['config']['mailer']['from']['name']);
$mail->addAddress($GLOBALS['config']['mailer']['to']['email'], $GLOBALS['config']['mailer']['to']['name']);
$mail->Subject = $GLOBALS['config']['mailer']['subject'];

$mail_body = '';

foreach ($fields as $field => $value) {
    $mail_body .= sprintf("%-15s:%s\n", $field, $value);
}

$mail->Body = $mail_body;

if (!$mail->send()) {
    include 'templates/error.tpl.php';
} else {
    include 'templates/success.tpl.php';
}