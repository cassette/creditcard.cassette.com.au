<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');


try {
    $db = new PDO(
        'mysql:host='.$GLOBALS['config']['db']['host'].
            ';dbname='.$GLOBALS['config']['db']['name'].';charset=utf8',
        $GLOBALS['config']['db']['user'],
        $GLOBALS['config']['db']['pass']
    );

    $query = $db->prepare('INSERT INTO '.$GLOBALS['config']['db']['table'].' (date, name, type, amount, job_number, details) VALUES (:date, :name, :type, :amount, :job_number, :details)');

    $query->execute($fields);

} catch(Exception $ex) {

}