<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Credit Card Log</title>
    <link rel="stylesheet" media="all" href="css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" media="all" href="css/all.css" type="text/css"/>
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.main.js"></script>
    <script type="text/javascript">
        if (navigator.userAgent.match(/IEMobile\/10\.0/) || navigator.userAgent.match(/MSIE 10.*Touch/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto !important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
    </script>
</head>
<body>
<div class="container">