<?php include 'templates/header.tpl.php' ?>

    <div class="submit-form-holder">
        <?php include 'templates/form.tpl.php' ?>
    </div>

    <form class="search-form" action="">
        <fieldset>
            <div class="bar">
                <input class="search" type="search" placeholder="Search">
                <input class="btn submit" type="submit" value="Search">
            </div>

            <div class="results-table-holder">
                <?php include 'templates/table.tpl.php' ?>
            </div>
        </fieldset>
    </form>

<?php include 'templates/footer.tpl.php' ?>