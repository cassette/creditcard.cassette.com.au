<?php if (is_array($table)) : ?>
<div class="table-responsive">
    <table class="table">
        <tr>
            <th>Date <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
            <th>Full Name <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
            <th>Card Type <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
            <th>Amount <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
            <th>Job Number <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
            <th>Details <a href="#"><span class="glyphicon glyphicon-chevron-down"></span></a></th>
        </tr>
        <?php foreach ($table as $row) : ?>
        <tr>
            <td><?php echo $row['date']?></td>
            <td><?php echo $row['name']?></td>
            <td><?php echo $row['type']?></td>
            <td><?php echo $row['amount']?></td>
            <td><?php echo $row['job_number']?></td>
            <td><?php echo $row['details']?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
<?php endif; ?>