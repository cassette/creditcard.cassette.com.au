<?php

defined('PROTECT_INCLUDES') or die('Do not access this file directly!');


try {
    $db = new PDO(
        'mysql:host='.$GLOBALS['config']['db']['host'].
            ';dbname='.$GLOBALS['config']['db']['name'].';charset=utf8',
        $GLOBALS['config']['db']['user'],
        $GLOBALS['config']['db']['pass']
    );

    $query = $db->prepare('SELECT * FROM '.$GLOBALS['config']['db']['table']);

    $query->execute();

    $table = $query->fetchAll();

} catch(Exception $ex) {
    echo $ex;
}